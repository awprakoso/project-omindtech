import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'

//Screen
import AppStack from './src/navigator/AppStack'
import LoginScreen from './src/screens/LoginScreen'
import ProfileScreen from './src/screens/ProfileScreen'
import DiscussionStack from './src/navigator/DiscussionStack'
import MainNavigator from './src/navigator/MainNavigator';

//redux
import {Provider} from 'react-redux';
import store from './src/redux/store'

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle="light-content" backgroundColor='#0BB5FF'/>
      <SafeAreaView style={{flex: 1}}>
        <NavigationContainer>
          <AppStack/>
        </NavigationContainer>
      </SafeAreaView>
    </Provider>
  );
};

export default App;
