import React from 'react';
import {View, Text, StyleSheet} from 'react-native'

const HomeScreen = () => {
    return (
        <View style={{alignItems: 'center', justifyContent: 'center', height: '100%'}}>
            <Text>Ini adalah halaman utama</Text>
        </View>
    )
}

export default HomeScreen;