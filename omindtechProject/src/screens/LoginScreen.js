import React, {useState} from 'react';
import {View, TextInput, TouchableOpacity, Text, StyleSheet} from 'react-native'
import {connect} from 'react-redux'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const LoginScreen = (props) => {
    const [email, setEmail] = useState('tentor@gmail.com')
    const [password, setPassword] = useState('123123')

    const handleLogin = () => {
        if(!email){
            alert('Email is required')
        }
        else if(!password){
            alert('Password is required')
        }
        else {
            props.apiLogin({
                email,
                password
            })
        }
    }

    const LoginButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapper}
            onPress={() => handleLogin()}>
                <Text style={styles.loginTextLabel}>Login</Text>
            </TouchableOpacity>
        )
    }

    const LoadingButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapperLoading}>
                <Text style={styles.loginTextLabel}>Loading...</Text>
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.backgroundScreen}>
            <View style={styles.container}>
                <Text style={styles.title}>Tentor Login</Text>
                <View>
                    <Text style={styles.inputLabel}>Email</Text>
                    <TextInput
                    placeholder='Input your email'
                    placeholderTextColor='#C4C4C4'
                    value={email}
                    style={styles.input}
                    onChangeText={(email) => setEmail(email)}
                    />

                    <Text style={styles.inputLabel}>Password</Text>
                    <TextInput
                    placeholder='Input your password'
                    placeholderTextColor='#C4C4C4'
                    value={password}
                    style={styles.input}
                    onChangeText={(password) => setPassword(password)}
                    />
                </View>
                { props.loading ? <LoadingButton /> : <LoginButton />}
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    backgroundScreen: {
        backgroundColor: '#0BB5FF',
        height: hp('100%'),
        width: wp('100%'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        width: wp('80%'),
        height: hp('50%'),
        margin: 16,
        marginTop: 10,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        borderRadius: 15,
        backgroundColor: '#ffffff',
        elevation: 15,
        alignItems: 'center',
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        marginVertical: hp('4%')
    },
    inputLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    input: {
        fontSize: 14,
        width: wp('60%'),
        paddingVertical: 5,
        marginBottom: 19,
        backgroundColor: '#f5f5f5',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        color: '#111111'
    },
    buttonWrapper: {
        backgroundColor: '#00BFFF',
        width: wp('30%'),
        padding: 5,
        borderRadius: 30,
        marginVertical: hp('2%'),
        alignItems: 'center'
    },
    buttonWrapperLoading: {
        backgroundColor: '#0099CC',
        width: wp('30%'),
        padding: 8,
        borderRadius: 30,
        marginVertical: hp('2%'),
        alignItems: 'center'
    },
    loginTextLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff'
    }
})

const stateToProps = (state) => ({
    loading: state.login.isLoading
})

const dispatchToProps = (dispatch) => ({
    apiLogin: (data) => dispatch({type: 'LOGIN', data})
})

export default connect(stateToProps, dispatchToProps)(LoginScreen)