import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Image} from 'react-native'
import {Picker} from '@react-native-picker/picker';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

//image
const options = {
    title: 'Select Avatar',
    storageOptions: {
      privateDirectory: true,
      skipBackup: true,
      path: 'images',
    },
};

const CreateGroupScreen = (props) => {
    const [nama, setNama] = useState('');
    const [jenjang, setJenjang] = useState('');
    const [image, setImage] = useState();
    const [rawImage, setRawImage] = useState({});
    
    function sendPost() {
        if (!nama) {
            alert('Masukkan nama grup anda');
        } else if (!jenjang) {
            alert('Pilih jenjang sekolah');
        } else {
            //process post
            const token = props.token;
            const dataPost =  new FormData() 
            dataPost.append('nama', nama)
            dataPost.append('kelas_id', jenjang)
            dataPost.append('thumbnail', rawImage)

            props.processPost({
                dataPost,
                token
            })
        }
    }

    function pickImage () {
        ImagePicker.showImagePicker(options, (response) => {
        console.info('Response = ', response);

        if (response.didCancel) {
            console.info('User cancelled image picker');
        } else if (response.error) {
            console.info('Image picker error: ', response.error);
        } else {
            const source = {
                uri: response.uri,
                type: response.type,
                data: response.data,
                name: response.fileName
            };

            setRawImage(source)
            setImage(response.uri)
        }
        });
    }

    console.log('ini token', props.token)

    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                {image == null ? 
                    <TouchableOpacity 
                    style={styles.imagePicker}
                    onPress={() => pickImage()}
                    >
                        <Ionicons name='camera' color='gray' size={25}/>
                    </TouchableOpacity>
                    :
                    <Image source={{uri: image}} style={styles.image}/>
                }
                
                <Text style={{color: '#C4C4C4', marginTop: 10}}>Tambahkan foto grup</Text>
            </View>

            <View style={{marginVertical: 10, marginHorizontal: wp('10%')}}>
                <TextInput 
                placeholder='Nama Grup'
                placeholderTextColor='#C4C4C4'
                value={nama}
                style={styles.input}
                onChangeText={(nama) => setNama(nama)}/>

                <View style={styles.pickerContainer}>
                    <Picker
                    selectedValue={jenjang}
                    style={styles.picker}
                    mode="dropdown"
                    onValueChange={(jenjang) => setJenjang(jenjang)
                    }>
                        <Picker.Item label="Jenjang" value="jenjang" />
                        <Picker.Item label="SD" value='1' />
                        <Picker.Item label="SMP" value='2' />
                        <Picker.Item label="SMA" value='3' />
                    </Picker>
                </View>

                <TouchableOpacity
                style={styles.buttonWrapper}
                onPress={() => {
                    sendPost()
                    props.navigation.navigate('Diskusi')
                }}>
                    <Text style={styles.buttonText}>Buat Grup</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF', 
        width: '100%', 
        height: hp('100%')
    },
    imageContainer: {
        alignSelf: 'center', 
        alignItems: 'center', 
        marginTop: 30, 
        marginBottom: 20
    },
    imagePicker: {
        width: wp('20%'),
        height: hp('10%'), 
        borderRadius: 5,
        backgroundColor: '#F5F5F5',
        alignItems: 'center', 
        justifyContent: 'center'
    },
    image: {
        width: wp('20%'),
        height: hp('10%'), 
        borderRadius: 5,
    },
    input: {
        fontSize: 14,
        width: wp('70%'),
        padding: 5,
        marginBottom: 19,
        backgroundColor: '#f5f5f5',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        color: '#111111',
        marginLeft: wp('5%')

    },
    pickerContainer: {
        height: hp('4.6%'),
        width: wp('37%'),
        borderColor: '#C4C4C4',
        borderWidth: 1,
        marginLeft: wp('5%'),
        backgroundColor: '#f5f5f5',
        borderRadius: 5
    },
    picker: {
        height: hp('4%'), 
        width: wp('36%'), 
        color: '#333333'
    },
    buttonWrapper: {
        backgroundColor: '#00BFFF',
        width: wp('70%'),
        alignSelf: 'center',
        padding: 8,
        marginTop: hp('5%'),
        borderRadius: 5,
        alignItems: 'center'
    },
    buttonText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#F1F1F1'
    }
})

const stateToProps = (state) => ({
    token: state.login.data.token_access
})

const dispatchToProps = (dispatch) => ({
    processPost: (data) => dispatch({type: 'CREATE_GROUP', data})
})

export default connect(stateToProps, dispatchToProps)(CreateGroupScreen);