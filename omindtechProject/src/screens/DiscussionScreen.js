import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {connect} from 'react-redux'

const DiscussionScreen = (props) => {
    console.log('group data screen', props.group)
    return (
        <View style={styles.container}>
            <View style={styles.groupContainer}>
                <Text style={styles.title}>Daftar grup diskusi anda: </Text>
                <View>
                {props.group == null ? 
                    <Text style={styles.nullText}>Anda tidak memiliki grup diskusi</Text>
                    :
                    <View style={styles.contentContainer}>
                        <Text>Nama grup: {props.group.nama}</Text>  
                    </View>
                }
                </View>
            </View>
            <TouchableOpacity
            style = {styles.button}
            onPress={() => props.navigation.navigate('Create Group')}
            >
                <Text style={styles.buttonText}>Buat grup</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: hp('100%'), 
        backgroundColor: '#FFFFFF', 
        alignItems: 'center'
    },
    groupContainer: {
        borderColor: '#C4C4C4', 
        borderWidth: 1, 
        marginVertical: wp('5%'), 
        borderRadius: 5, 
        padding: 5
    },
    contentContainer: {
        
    },
    title: {
        marginBottom: 20, 
        fontWeight: 'bold'
    },
    nullText: {
        alignSelf: 'center', 
        color: '#C4C4C4', 
        marginHorizontal: 10
    },
    button: {
        width: wp('30%'),
        backgroundColor: '#00BFFF',
        padding: 8,
        borderRadius: 5,
        margin: 10,
        alignItems: 'center'
    },
    buttonText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#F1F1F1'
    }
})
const stateToProps = (state) => ({
    group: state.createGroup.groupData
})

const dispatchToProps = (dispatch) => ({
    
})

export default connect(stateToProps, dispatchToProps)(DiscussionScreen);