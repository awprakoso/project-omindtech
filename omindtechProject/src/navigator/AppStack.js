import React from 'react';
import {connect} from 'react-redux'

//Navigator
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

//Screen
import LoginScreen from '../screens/LoginScreen';
import MainNavigator from './MainNavigator'

function AppStack(props){
    return (
        <Stack.Navigator headerMode='none'>  
            {props.verify ? 
                <Stack.Screen name='Home' component={MainNavigator}/> 
                :
                <Stack.Screen name='Login' component={LoginScreen}/> 
            }   
        </Stack.Navigator>
    )
}

const stateToProps = (state) => ({
    verify: state.login.isLoggedIn
})

const dispatchToProps = (dispatch) => ({
    
})

export default connect(stateToProps, dispatchToProps)(AppStack)