import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {View} from 'react-native';
import {connect} from 'react-redux'

//Screen to navigate
import HomeScreen from '../screens/HomeScreen'
import LiveScreen from '../screens/LiveScreen'
import DiscussionStack from './DiscussionStack'
import ProfileScreen from '../screens/ProfileScreen'

//Bottom Tab Navigator
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
    return (
        <Tab.Navigator
            screenOptions = {({route}) => ({
                tabBarIcon: ({color, size}) => {
                    let iconName;

                    if (route.name === 'Home'){
                        iconName = 'home-outline'
                    } else if (route.name === 'Live'){
                        iconName = 'radio-outline'
                    } else if (route.name === 'Diskusi'){
                        iconName = 'chatbubbles-outline'
                    } else if (route.name === 'Profile'){
                        iconName = 'person-circle-outline'
                    }
                    return <Ionicons name={iconName} size={size} color={color}/>
                }
            })}
            tabBarOptions={{
                activeTintColor: '#00BFFF',
                inactiveTintColor: '#808080'
            }}
            >
            <Tab.Screen name='Home' component={HomeScreen}/>
            <Tab.Screen name='Live' component={LiveScreen}/>
            <Tab.Screen name='Diskusi' component={DiscussionStack}/>
            <Tab.Screen name='Profile' component={ProfileScreen}/>
        </Tab.Navigator>
    )
}

const stateToProps = (state) => ({
   
})

const dispatchToProps = (dispatch) => ({
    
})

export default connect(stateToProps, dispatchToProps)(MainNavigator)