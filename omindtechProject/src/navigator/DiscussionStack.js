import React from 'react';

//Navigator
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

//Screen
import DiscussionScreen from '../screens/DiscussionScreen';
import CreateGroupScreen from '../screens/CreateGroupScreen'

const DiscussionStack = (props) => {
    return (
        <Stack.Navigator >
            <Stack.Screen name='Diskusi' component={DiscussionScreen} options={{headerShown: false}} />
            <Stack.Screen name='Create Group' component={CreateGroupScreen} options={{title: 'Buat Grup'}}/>
        </Stack.Navigator>
    )
}

export default DiscussionStack;