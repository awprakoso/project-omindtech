import {combineReducers} from 'redux'
import login from './auth';
import createGroup from './createGroup'

export default combineReducers({
    login, createGroup
})