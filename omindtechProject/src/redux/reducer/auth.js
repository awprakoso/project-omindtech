const initialState = {
    isLoading: false,
    isLoggedIn: false,
    data: {}
}

const login = (state = initialState, action) => {
    switch(action.type){
        case 'LOGIN': {
            return{
                ...state,
                isLoading: true
            }
        }
        case 'LOGIN_SUCCESS': {
            return{
                ...state,
                isLoading: false,
                isLoggedIn: true,
                data: action.data
            }
        }
        case 'LOGIN_FAILED': {
            return{
                ...state,
                isLoading: false,
            }
        }
        case 'LOGOUT': {
            return{
                ...state,
                isLoading: false,
                isLoggedIn: false
            }
        }
        default:
            return state;
    }
}

export default login;