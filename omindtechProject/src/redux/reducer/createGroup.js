const initialState = {
    isLoading: false,
    groupData: {}
}

const createGroup = (state = initialState, action) => {
    switch(action.type){
        case 'CREATE_GROUP': {
            return{
                ...state,
                isLoading: true
            }
        }
        case 'CREATE_GROUP_SUCCESS': {
            return{
                ...state,
                isLoading: false,
                groupData: action.data
            }
        }
        case 'CREATE_GROUP_FAILED': {
            return{
                ...state,
                isLoading: false,
            }
        }
        default:
            return state;
    }
}

export default createGroup;