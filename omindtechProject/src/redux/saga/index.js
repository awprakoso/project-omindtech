import {all} from 'redux-saga/effects'
import loginSaga from './auth'
import createGroupSaga from './createGroup'

export default function* rootSaga(){
    yield all([loginSaga(), createGroupSaga() ])
}