import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios'
import {ToastAndroid} from 'react-native'

function* login(action){
    try {
        const loginResponse = yield axios({
            method: 'POST',
            url: 'http://bta70.omindtech.id/api/tentor/login',
            data: action.data
        })

        yield put({type: 'LOGIN_SUCCESS', data: loginResponse.data.data})

        ToastAndroid.show(
            'Login berhasil',
        ToastAndroid.SHORT)
    }
    catch (err) {
        console.log(err)
        yield put({type: 'LOGIN_FAILED'})

        ToastAndroid.show(
            'Login gagal',
        ToastAndroid.SHORT)

    }
}

function* logout(){
    try {

    }
    catch (err) {
        console.log(err)
    }
}

function* loginSaga(){
    yield takeLatest('LOGIN', login)
    yield takeLatest('LOGOUT', logout)
}

export default loginSaga