import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios'
import {ToastAndroid} from 'react-native'

function* createGroup(action){
    try {
        console.log('token saga', action.data.token)
        console.log('dataPost', action.data.dataPost)
        const createPostResponse = yield axios({
            method: 'POST',
            url: 'http://bta70.omindtech.id/api/grup',
            data: action.data.dataPost,
            headers: {
                'Content-Type': 'application/json',
                Authorization: action.data.token,
            }
        })

        yield put({type: 'CREATE_GROUP_SUCCESS', data: createPostResponse.data.data})

        ToastAndroid.show(
            'Berhasil membuat grup',
        ToastAndroid.SHORT)
    }
    catch (err) {
        console.log(err)
        yield put({type: 'CREATE_GROUP_FAILED'})

        ToastAndroid.show(
            'Gagal membuat grup',
        ToastAndroid.SHORT)

    }
}

function* createGroupSaga(){
    yield takeLatest('CREATE_GROUP', createGroup)
}

export default createGroupSaga;